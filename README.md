## :ledger: vas-tel

> Developing a Todo app using feathersjs framework in this project there are two entities

- Users
- Tasks
  The relationship between both entity one to many One user has Many tasks.

## About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

### Assumption

In the DB Design i assumed that this app will serve alot of users that's why i used reverse referencing and also indexing the userId for faster retrevial,search or update operations.

### Database Design

![Database design](https://i.ibb.co/sWVZhr8/Untitled-3.jpg)

## Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Pull the project by running the following command `git clone https://gitlab.com/AhmedAbdelfatah/vastel.git`

3. Install your dependencies

   ```
   cd vastel
   npm install
   ```

4. Make sure that the `.env` file contains the following vars

   ```
   HOST=localhost
   PORT=3030
   JWT_SECRET=PUT-HERE-RANDOM-STRING
   MONGODB_URL=PUT-HERE-MONGO-DB-URL
   ```

5. Start your app in development mode by running the following command

   ```

   npm run dev

   ```

## Important Links

- [POC](https://www.youtube.com/watch?v=cFD6OkSD54A)
- [Post-Man collection](https://api.postman.com/collections/1943064-5780c4be-fd24-4455-a702-ce04c4c0edbd?access_key=PMAT-01GRHAYZEYFNVZF5GQ508R2SX0)

## Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```

$ npm install -g @feathersjs/cli # Install Feathers CLI

$ feathers generate service # Generate a new Service
$ feathers generate hook # Generate a new Hook
$ feathers help # Show all commands

```
