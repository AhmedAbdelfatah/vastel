module.exports = {};
module.exports = {
  EMPTY_NAME: "Task name can not be empty",
  EMPTY_DESCRIPTION: "Task description can not be empty",
  TASK_NAME_MIN_LENGTH: "Task name length must be 8 chars or more!",
  TASK_DESCRIPTION_MIN_LENGTH:
    "Task description length must be 20 chars or more!",
  USER_NAME_MIN_LENGTH: "Name length must be 8 chars or more!",
};
