// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async function nosqlInjectionFilter(context) {
    if (!context.data) {
      return context;
    }

    Object.keys(context.data).forEach((key) => {
      if (typeof context.data[key] === "string") {
        context.data[key] = context.data[key].replace(/[^\w\s]/gi, "");
      }
    });

    return context;
  };
};
