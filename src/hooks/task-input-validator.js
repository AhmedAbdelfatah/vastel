const { EMPTY_DESCRIPTION, EMPTY_NAME } = require("./../constants/errorMsg");
// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return (context) => {
    const {
      data: { name, description },
    } = context;
    if (name && name.trim() === "") {
      throw new Error(EMPTY_NAME);
    }
    if (description && description.trim() === "") {
      throw new Error(EMPTY_DESCRIPTION);
    }
  };
};
