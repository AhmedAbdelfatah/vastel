const xss = require("xss");
// eslint-disable-next-line no-unused-vars
module.exports = (options = {}) => {
  return async function xssFilter(context) {
    if (!context.data) {
      return context;
    }

    Object.keys(context.data).forEach((key) => {
      if (typeof context.data[key] === "string") {
        context.data[key] = xss(context.data[key]);
      }
    });

    return context;
  };
};
