const DAY_TO_MS = 24 * 60 * 60 * 1000;
const DEFAULT_DEADLINE = Date.now() + DAY_TO_MS;
const {
  TASK_NAME_MIN_LENGTH,
  TASK_DESCRIPTION_MIN_LENGTH,
} = require("./../constants/errorMsg");
const taskStatus = require("./../constants/taskStatus");
module.exports = function (app) {
  const modelName = "tasks";
  const mongooseClient = app.get("mongooseClient");
  const { Schema } = mongooseClient;
  const schema = new Schema(
    {
      name: {
        type: String,
        required: true,
        unique: true,
        minlength: [8, TASK_NAME_MIN_LENGTH],
        maxlength: 50,
      },
      description: {
        type: String,
        required: true,
        minlength: [20, TASK_DESCRIPTION_MIN_LENGTH],
        maxlength: 255,
      },
      deadline: { type: Number, default: DEFAULT_DEADLINE },
      status: {
        type: String,
        enum: Object.values(taskStatus),
        default: taskStatus.onProgress,
      },

      user: {
        type: Schema.Types.ObjectId,
        ref: "users",
      },
    },
    {
      timestamps: true,
    }
  );
  schema.index({ user: 1 });
  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
};
