const { USER_NAME_MIN_LENGTH } = require("./../constants/errorMsg");
module.exports = function (app) {
  const modelName = "users";
  const mongooseClient = app.get("mongooseClient");
  const schema = new mongooseClient.Schema(
    {
      name: {
        type: String,
        unique: true,
        lowercase: true,
        required: true,
        minlength: [8, USER_NAME_MIN_LENGTH],
        maxlength: 30,
      },
      email: { type: String, unique: true, lowercase: true, required: true },
      password: {
        type: String,
        required: true,
      },
    },
    {
      timestamps: true,
    }
  );

  if (mongooseClient.modelNames().includes(modelName)) {
    mongooseClient.deleteModel(modelName);
  }
  return mongooseClient.model(modelName, schema);
};
