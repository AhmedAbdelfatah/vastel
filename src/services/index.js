const users = require("./users/users.service.js");
const tasks = require("./tasks/tasks.service.js");
module.exports = function (app) {
  app.configure(users);
  app.configure(tasks);
};
