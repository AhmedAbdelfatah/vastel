const { authenticate } = require("@feathersjs/authentication").hooks;
const preCreate = require("./../../utils/preCreate");
const { setField } = require("feathers-authentication-hooks");
const xssFilter = require("./../../hooks/xss-filter");
const nosqlInjectionFilter = require("./../../hooks/nosqlinjection-filter");
const inputValidator = require("./../../hooks/task-input-validator");
const limitToUser = setField({
  from: "params.user._id",
  as: "params.query.user",
});
module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [limitToUser],
    get: [limitToUser],
    create: [preCreate, xssFilter(), nosqlInjectionFilter(), inputValidator()],
    update: [
      limitToUser,
      xssFilter(),
      nosqlInjectionFilter(),
      inputValidator(),
    ],
    patch: [limitToUser, xssFilter(), nosqlInjectionFilter(), inputValidator()],
    remove: [limitToUser],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
