const { Tasks } = require("./tasks.class");
const createModel = require("../../models/tasks.model");
const hooks = require("./tasks.hooks");

module.exports = async function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get("paginate"),
    lean: true,
    multi: ["remove"],
  };

  app.use("/tasks", new Tasks(options, app));

  const service = app.service("tasks");
  service.hooks(hooks);
};
