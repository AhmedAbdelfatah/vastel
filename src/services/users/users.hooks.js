const { authenticate } = require("@feathersjs/authentication").hooks;
const { hashPassword, protect } =
  require("@feathersjs/authentication-local").hooks;
const { setField } = require("feathers-authentication-hooks");

const limitToUser = setField({
  from: "params.user._id",
  as: "params.query._id",
});
module.exports = {
  before: {
    all: [],
    find: [authenticate("jwt")],
    get: [authenticate("jwt"), limitToUser],
    create: [hashPassword("password")],
    update: [hashPassword("password"), authenticate("jwt"), limitToUser],
    patch: [hashPassword("password"), authenticate("jwt"), limitToUser],
    remove: [authenticate("jwt"), limitToUser],
  },

  after: {
    all: [protect("password")],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [
      async (context) => {
        const user = context.result._id;
        await context.app.service("tasks").remove(null, {
          query: { user: user },
        });
      },
    ],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
