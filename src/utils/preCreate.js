const preCreate = async (context) => {
  const { data } = context;
  const { user } = context.params;
  data.user = user._id;
  return context;
};
module.exports = preCreate;
